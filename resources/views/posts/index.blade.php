@extends('layouts.app')

@section('content')
<style>
    body{
        background-color:#12bac5;
        color: white;
    }
    .well{
        color: white;
        margin: 16px 0;
        padding: 16px;
        background-color: #92267a;
        border-radius:8px; 
    }
    .well h3 a{
        color:#ffffff;/* #931621; 473198*/
    }
</style>
    <h1 style="font-weight: 900;text-align:center;">Posts</h1>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="well">
                <h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                <p style="max-height:24px; white-space: nowrap; width:1024px; overflow:hidden; text-overflow:ellipsis; ">{{$post->body}}...</p>
                <h6 style="text-align:right">Written on {{$post->created_at}} by {{$post->user->name}} </h6>
            </div>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No post found</p>
    @endif
@endsection