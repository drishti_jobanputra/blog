@extends('layouts.app')

@section('content')
<style>
    body{
        background-color:#577783;
        color: white;
    }
</style> 
    <a href="/posts" class="btn btn-default" style="background-color: #70EE9C; color:black;">Go Back</a><br><br>
    <h1>{{$post->title}}</h1>
    <div>
        {!!$post->body!!}
    </div>
    <hr>
    <small>Written on {{$post->created_at}}</small>
    <hr>
    @if(Auth::check())
        @if(Auth::user()->id == $post->user_id)
    
            <a href="/posts/{{$post->id}}/edit" class="btn btn-default" style="background-color: #70EE9C">Edit</a><br><br>

            {!!Form::open(['action' => ['\App\Http\Controllers\PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right' ])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection