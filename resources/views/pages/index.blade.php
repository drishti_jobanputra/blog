@extends('layouts.app')

@section('content')
<style>
        body{
                background-color: #0da5b2;
                color: white;
        }
        .grid-container{
                display: grid;
                grid-template-columns: 1fr 1fr;

        }
        .grid-item-image{
                margin-right: 40px;
        }
</style>
<div class="grid-container">
        <div class="grid-item-image">
                <img src="/images/blog_homepage.jpeg">
        </div>
        <div class="grid-item-content" style="margin-left: 20px">
                <br><br><br><br><br><br><br><br><h1>Welcome to Collabrains!</h1><br>
                <h2>This is a Forum where you can set up your own blog.</h2><br>
                <h2 style="text-align: center">Happy Blogging!</h2><br>
                {{-- <p>
                   <a class="btn btn-primary btn-lg" href="/login" role="button">Login</a>
                   <a class="btn btn-success btn-lg" href="/register" role="button">Register</a>
                </p> --}}
        </div>
@endsection